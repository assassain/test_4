/*
 * File: statement.cpp
 * -------------------
 * This file implements the constructor and destructor for
 * the Statement class itself.  Your implementation must do
 * the same for the subclasses you define for each of the
 * BASIC statements.
 */

#include <string>
#include "statement.h"
using namespace std;

/* Implementation of the Statement class */

bool resexist(string str){
    return (str == "REM") || (str == "LET") || (str == "PRINT") || (str == "INPUT") 
        || (str == "END") || (str == "GOTO") || (str == "IF") || (str == "THEN") || (str == "RUN") 
        || (str == "LIST") || (str == "CLEAR") || (str == "QUIT") || (str == "HELP");
}

Statement::Statement() {
   /* Empty */
}

Statement::~Statement() {
   /* Empty */
}
void LETstatement :: Check(){
   
	TokenScanner scanner;
    scanner.ignoreWhitespace();
    scanner.scanNumbers();
    scanner.setInput(line);

    string st = scanner.nextToken();
    if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    if (scanner.getTokenType(st) == NUMBER)
	{
      string tmp = scanner.nextToken();
      if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    }

    string varp = scanner.nextToken();
    if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    if (resexist(varp)) error("SYNTAX ERROR");
    if (scanner.getTokenType(varp) != WORD) error("SYNTAX ERROR");
    
	string oper = scanner.nextToken();
    if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    if (oper != "=") error("SYNTAX ERROR");
    
    this->exp = parseExp(scanner);
    if (exp == nullptr) error("SYNTAX ERROR");
    this->var = varp;
    return;
}

void PRINTstatement :: Check(){
    TokenScanner scanner;
    scanner.ignoreWhitespace();
    scanner.scanNumbers();
    scanner.setInput(line);

    string st = scanner.nextToken();
    if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    if (scanner.getTokenType(st) == NUMBER){
        string next = scanner.nextToken();
        if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    }
    this->exp = parseExp(scanner);
    if (exp == nullptr) error("SYNTAX ERROR");
    return;
}

void INPUTstatement :: Check(){
    TokenScanner scanner;
    scanner.ignoreWhitespace();
    scanner.scanNumbers();
    scanner.setInput(line);

    string st = scanner.nextToken();
    if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    if (scanner.getTokenType(st) == NUMBER){
        string next = scanner.nextToken();
        if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    }

    string varp = scanner.nextToken();
    if (scanner.hasMoreTokens()) error("SYNTAX ERROR");
    if (resexist(varp)) error("SYNTAX ERROR");
    if (scanner.getTokenType(varp) != WORD) error("SYNTAX ERROR");
    this->var = varp;
    return;
}
bool isNumber(string str){
    if (!isdigit(str[0]) && str[0] != '-') return false;
    for (int i = 1; i < str.size(); i++) if (!isdigit(str[i])) return false;
    return true;
}
int INPUTstatement :: Execute(EvalState & state){ 
    string input;
    while (1)
	{
      cout << " ? ";
      getline(cin, input);
      if (isNumber(input)) break;
      cout << "INVALID NUMBER" << endl;
    }
    state.setValue(var, atoi(input.data()));
    return -1;
}

void ENDstatement :: Check(){
    TokenScanner scanner;
    scanner.ignoreWhitespace();
    scanner.scanNumbers();
    scanner.setInput(line);

    string st = scanner.nextToken();
    if (scanner.getTokenType(st) == NUMBER)
        string next = scanner.nextToken();
    if (scanner.hasMoreTokens()) error("SYNTAX ERROR");
    return;
}

void GOTOstatement :: Check(){
    TokenScanner scanner;
    scanner.ignoreWhitespace();
    scanner.scanNumbers();
    scanner.setInput(line);

    string st = scanner.nextToken();
    if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    if (scanner.getTokenType(st) == NUMBER){
        string next = scanner.nextToken();
        if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    }

    string line_number = scanner.nextToken();
    if (scanner.hasMoreTokens()) error("SYNTAX ERROR");
    if (scanner.getTokenType(line_number) != NUMBER) error("SYNTAX ERROR");
    this->destination = atoi(line_number.data());
    return;
}

void IFstatement :: Check(){
    TokenScanner scanner;
    scanner.ignoreWhitespace();
    scanner.scanNumbers();
    scanner.setInput(line);

    string st = scanner.nextToken();
    if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    if (scanner.getTokenType(st) == NUMBER){
        string next = scanner.nextToken();
        if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    }

    TokenScanner scanner1, scanner2;
    while (scanner.hasMoreTokens() ){
        string tmp = scanner.nextToken();
        if (tmp == "=" || tmp == ">" || tmp == "<")
		{
            this->cmp = tmp;
            break;
        }
        scanner1.saveToken(tmp);
    }
    if (!scanner1.hasMoreTokens()) error("SYNTAX ERROR");
    if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
   while (scanner.hasMoreTokens()){
        string tmp = scanner.nextToken();
        if (tmp == "THEN") break;
        scanner2.saveToken(tmp);
    }
    if (!scanner2.hasMoreTokens()) error("SYNTAX ERROR");
    if (!scanner.hasMoreTokens()) error("SYNTAX ERROR");
    string line_number = scanner.nextToken();
    if (scanner.hasMoreTokens()) error("SYNTAX ERROR");
    if (scanner.getTokenType(line_number) != NUMBER) error("SYNTAX ERROR");

    TokenScanner inverse1, inverse2;
    for (; scanner1.hasMoreTokens(); ) inverse1.saveToken(scanner1.nextToken());
    for (; scanner2.hasMoreTokens(); ) inverse2.saveToken(scanner2.nextToken());

    this->exp1 = parseExp(inverse1);
    this->exp2 = parseExp(inverse2);
    if (exp1 == nullptr || exp2 == nullptr) error("SYNTAX ERROR");
    this->destination = atoi(line_number.data());
    return;
}
int IFstatement :: Execute(EvalState & state){
    int value1 = exp1->eval(state);
    int value2 = exp2->eval(state);
    if (cmp == "="){
        return value1 == value2 ? destination : -1;
    }else if (cmp == "<"){
        return value1 < value2 ? destination : -1;
    }else if (cmp == ">"){
        return value1 > value2 ? destination : -1;
    }
}

